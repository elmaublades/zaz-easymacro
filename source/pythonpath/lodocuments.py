#!/usr/bin/env python3

from net.elmau.zaz.EasyMacro import XLODocuments
import easymacro as app


class LODocuments(XLODocuments):

    def new_doc(self, type_doc, kwargs):
        if not type_doc:
            type_doc = app.CALC
        opt = {}
        if kwargs:
            opt = app.array_to_dict(kwargs)
        doc = app.new_doc(type_doc, **opt)
        return doc.obj

    def new_db(self, path):
        doc = app.new_db(path)
        return doc.obj

    def open_doc(self, path, kwargs):
        opt = {}
        if kwargs:
            opt = app.array_to_dict(kwargs)
        doc = app.open_doc(path, **opt)
        return doc.obj

    def get_document(self, title):
        doc = app.get_document(title)
        if not doc is None:
            doc = doc.obj
        return doc

    def get_documents(self):
        return app.get_documents(False)

    def get_type_doc(self, doc):
        return app.get_type_doc(doc)

    def set_focus(self, doc):
        doc = app.LODocument(doc)
        doc.focus()
        return

    def set_visible(self, doc, visible):
        doc = app.LODocument(doc)
        doc.visible = visible
        return

    def set_zoom(self, doc, value):
        doc = app.LODocument(doc)
        doc.zoom = value
        return

    def get_statusbar(self, doc):
        doc = app.LODocument(doc)
        return doc.statusbar

    def to_pdf(self, doc, path, kwargs):
        opt = app.array_to_dict(kwargs)
        doc = app.LODocument(doc)
        path_pdf = doc.to_pdf(path, **opt)
        return path_pdf

    def paste(self, doc):
        doc = app.LODocument(doc)
        return doc.paste()

    def select(self, doc, rango):
        type_doc = app.get_type_doc(doc)
        cc = doc.getCurrentController()
        if type_doc == 'calc':
            cc.select(rango)
        return

    def selection(self, doc):
        return doc.getCurrentSelection()


