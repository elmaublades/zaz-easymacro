#!/usr/bin/env python3

from net.elmau.zaz.EasyMacro import XLOCalc
import easymacro as app


class LOCalc(XLOCalc):

    def get_sheet(self, doc, index):
        if not doc:
            doc = app.get_document().obj
        if not index == '':
            sheet = doc.Sheets[index]
        else:
            sheet = doc.getCurrentController().getActiveSheet()
        return sheet

    def sheet_activate(self, doc, sheet):
        doc.getCurrentController().setActiveSheet(sheet)
        return

    def sheet_exists(self, doc, name):
        return name in doc.Sheets

    def sheet_insert(self, doc, name, pos):
        doc = app.LOCalc(doc)
        return doc.insert(name, pos).obj

    def sheet_copy(self, source, source_names, target, target_names, pos):
        doc_source = app.LOCalc(source)
        if target:
            doc = app.LOCalc(target)
            sheet = doc.copy_from(doc_source, source_names, target_names, pos)
        else:
            sheet = doc_source.copy(source_names, target_names, pos)
        return sheet.obj

    def sheet_move(self, doc, name, pos):
        app.LOCalc(doc).sheets.move(name, pos)
        return

    def sheet_remove(self, doc, name):
        app.LOCalc(doc).sheets.remove(name)
        return

    def sheet_sort(self, doc, reverse):
        app.LOCalc(doc).sort(reverse)
        return

    def get_cell(self, address):
        if address:
            cell = app.get_cell(address)
        else:
            cell = app.get_cell()
        return cell.obj

    def get_range(self, obj, address):
        if not obj.ImplementationName == 'ScTableSheetObj':
            obj = obj.getCurrentController().getActiveSheet()

        if isinstance(address, tuple):
            if len(address) == 1:
                address = (slice(0, None), address[0])
            elif len(address) == 4:
                address = (
                    slice(address[0], address[1]),
                    slice(address[2], address[3])
                )
        return obj[address]

    def get_value(self, rango):
        v = None
        if rango.ImplementationName == app.OBJ_CELL:
            tc = rango.getType()
            if tc == app.VALUE:
                v = rango.getValue()
            elif tc == app.TEXT:
                v = rango.getString()
            elif tc == app.FORMULA:
                v = rango.getFormula()
        else:
            v = rango.getDataArray()
        return v

    def set_value(self, rango, value):
        if rango.ImplementationName == app.OBJ_CELL:
            if isinstance(value, str):
                if value.startswith('='):
                    rango.setFormula(value)
                else:
                    rango.setString(value)
            else:
                rango.setValue(value)
        else:
            rango.setDataArray(value)
        return

    def set_data(self, cell, data, formula):
        cursor = cell.getSpreadsheet().createCursorByRange(cell[0,0])
        cols = len(data[0])
        rows = len(data)
        cursor.collapseToSize(cols, rows)
        if formula:
            cursor.setFormulaArray(data)
        else:
            cursor.setDataArray(data)
        return

    def get_data(self, cell, visible, formula):
        cursor = cell.getSpreadsheet().createCursorByRange(cell[0,0])
        cursor.collapseToCurrentRegion()
        if visible:
            rangos = cursor.queryVisibleCells()
            data = ()
            for i in range(rangos.Count):
                if formula:
                    data += rangos.getByIndex(i).getFormulaArray()
                else:
                    data += rangos.getByIndex(i).getDataArray()
        else:
            if formula:
                data = cursor.getFormulaArray()
            else:
                data = cursor.getDataArray()
        return data

    def get_last_row(self, cell):
        cursor = cell.getSpreadsheet().createCursorByRange(cell[0,0])
        cursor.gotoEnd()
        return cursor.getRangeAddress().EndRow

    def get_current_region(self, cell):
        sheet = cell.getSpreadsheet()
        cursor = sheet.createCursorByRange(cell)
        cursor.collapseToCurrentRegion()
        return sheet[cursor.AbsoluteName]

    def get_next_cell(self, cell):
        sheet = cell.getSpreadsheet()
        cursor = sheet.createCursorByRange(cell)
        cursor.collapseToCurrentRegion()
        rows = cursor.Rows.Count
        cursor.gotoStart()
        cursor.gotoOffset(0, rows)
        return sheet[cursor.AbsoluteName]

    def get_visible(self, rango):
        cursor = rango.getSpreadsheet().createCursorByRange(rango)
        rangos = cursor.queryVisibleCells()
        return rangos

    def get_empty(self, rango):
        cursor = rango.getSpreadsheet().createCursorByRange(rango)
        rangos = cursor.queryEmptyCells()
        return rangos

    def create_cell_style(self, doc, name, properties):
        doc = app.LOCalc(doc)
        style = doc.create_cell_style(name)
        properties = app.array_to_dict(properties)
        doc.cell_style.apply(style, properties)
        return style

